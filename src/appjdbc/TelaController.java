/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appjdbc;

import appjdbc.model.DBUsuario;
import appjdbc.model.Usuario;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author aluno
 */
public class TelaController implements Initializable {
    
    @FXML
    private Label mensagemSistema;
    
    @FXML
    private TextField nomeCompleto, usuario, email, senha, reSenha;
    
    @FXML
    private TextField userSearch;
    
    @FXML
    private TableView tbUsers;
    
    @FXML
    private TableColumn colUser, colName;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        userSearch.setOnAction((ActionEvent e) -> onSearchTermChanged());
        colUser.setCellValueFactory(new PropertyValueFactory<>("username"));
        colName.setCellValueFactory(new PropertyValueFactory<>("name"));
    }    
    
    @FXML
    private void onAction(){
        Usuario u = new Usuario();
        
        u.setName(nomeCompleto.getText());
        u.setUsername(usuario.getText());
        u.setEmail(email.getText());
        u.setPassword(senha.getText());
        if(senha.getText().equals(reSenha.getText())){
            DBUsuario.adicionarUsuario(u);
        } else {
            mensagemSistema.setText("Senha não confere!");
        }
                
    }
    
    @FXML
    void onSearchTermChanged(){
        ArrayList<Usuario> searchResult = DBUsuario.search(userSearch.getText());
        System.out.println("SearchResult tem tamanho " + searchResult.size());
        tbUsers.setItems(FXCollections.observableArrayList(searchResult));
    }
    
}
