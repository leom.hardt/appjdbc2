/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appjdbc.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;
import java.sql.Statement;
import java.util.ArrayList;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;

/**
 *
 * @author aluno
 */
public class DBUsuario {
    
    public static void inicializar() throws Exception{
        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection conn = DriverManager.getConnection(
                "jdbc:oracle:thin:@oracle.canoas.ifrs.edu.br:1521:XE", Dados.getUsername(), Dados.getPassword()
        );
        
        PreparedStatement ps = conn.prepareStatement("CREATE TABLE Usuarios(nome varchar2(40), "
                + "username varchar2(40) primary key not null,"
                + "password varchar2(40),"
                + "email varchar2(40))");
        try{
            if(ps.execute()){
                throw new Exception("Não criou os dados");
            }
        }catch(SQLSyntaxErrorException e){
            // Se não for um erro por criar uma tabela existente
            if(! e.getLocalizedMessage().contains("ORA-00955")){
                throw e;
            }
        }catch(ClassNotFoundException e){
            System.out.println("Não carregou a classe...");
        }
    }
    
    public static void adicionarUsuario(Usuario u){
        try{
            inicializar();
            Connection conn = DriverManager.getConnection(
                    "jdbc:oracle:thin:@oracle.canoas.ifrs.edu.br:1521:XE", Dados.getUsername(), Dados.getPassword()
            );
            PreparedStatement ps = conn.prepareStatement(
                    "INSERT INTO Usuarios(nome, username, password, email) values (?,?,?,?)");
            ps.setString(1, u.getName());
            ps.setString(2, u.getUsername());
            ps.setString(3, u.getPassword());
            ps.setString(4, u.getEmail());
            
            int i = ps.executeUpdate();
            System.out.println(i);
        } catch(SQLException e){
            Dialog<String> d = new Dialog<>();
            d.getDialogPane().setContent(new Label("Usuário não adicionado. "  + e));
            d.getDialogPane().getButtonTypes().add(ButtonType.OK);
            d.showAndWait();
        } catch(Exception e){
            System.out.println("Exceção. " + e);
        }
    }
    
    public static boolean testLogin(){
        try{
            Connection conn = DriverManager.getConnection(
                    "jdbc:oracle:thin:@oracle.canoas.ifrs.edu.br:1521:XE", Dados.getUsername(), Dados.getPassword()
            );
            return true;
        } catch(SQLException e){
            return false;
        }
        
    }
    
    public static ArrayList<Usuario> getAll(){
        ArrayList<Usuario> users = new ArrayList<>();
        try{
            Connection conn = DriverManager.getConnection(
                    "jdbc:oracle:thin:@oracle.canoas.ifrs.edu.br:1521:XE", Dados.getUsername(), Dados.getPassword()
            );
            
            ResultSet res = conn.prepareStatement("SELECT * FROM Usuarios").executeQuery();
            while(res.next()){
                users.add(
                    new Usuario(
                         res.getString("nome"),
                         res.getString("username"),
                         res.getString("password"),
                         res.getString("email")
                    )
                );
            }
            
        } catch(SQLException e){
            System.err.println("exception " + e);
        }
        return users;
    }
    
    public static ArrayList<Usuario> search(String str){
        ArrayList<Usuario> users = new ArrayList<>();
        try{
            Connection conn = DriverManager.getConnection(
                    "jdbc:oracle:thin:@oracle.canoas.ifrs.edu.br:1521:XE", Dados.getUsername(), Dados.getPassword()
            );
            
            PreparedStatement ps = conn.prepareStatement("SELECT * FROM Usuarios where username LIKE ?");
            
            ps.setString(1, '%'  + str + '%');
          //  ps.setString(2, str);
            ResultSet res = ps.executeQuery();
            while(res.next()){
                users.add(
                    new Usuario(
                         res.getString("nome"),
                         res.getString("username"),
                         res.getString("password"),
                         res.getString("email")
                    )
                );
            }
            
        } catch(SQLException e){
            System.err.println("exception " + e);
        }
        return users;
    }
}
